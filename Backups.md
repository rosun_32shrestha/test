# Restore From Backup

## Restore
- Login to amazon aws and select the rds
- Select the database instance you want to restore. And under instance select the **Restore to Point** in Time point to be selected.
- Then you can select the given restore point or you can set it manually.
- In one click, you can launch new db instance and restore your database with the backup.
- Make sure that admin gives privileges to the new instance being created.


